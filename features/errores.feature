#language: es
Característica: Manejo de errores

  Antecedentes:
    Dado que mi cuit es "30445556667"

  Escenario: Inversion solo en plazo fijo con plazo demasiado largo
    Dado que el interes de plazo fijo es 10 % anual
    Cuando invierto $ 1000 en un plazo fijo a 500 dias
    Entonces obtengo un error "plazo_no_valido"

  Escenario: Inversion solo en plazo fijo con plazo demasiado corto
    Dado que el interes de plazo fijo es 10 % anual
    Cuando invierto $ 1000 en un plazo fijo a 5 dias
    Entonces obtengo un error "plazo_no_valido"

  Escenario: Inversion solo en plazo fijo con plazo interes negativo
    Dado que el interes de plazo fijo es -10 % anual
    Cuando invierto $ 1000 en un plazo fijo a 50 dias
    Entonces obtengo un error "interes_no_valido"