require 'sinatra'
require 'json'
Dir[File.join(__dir__, 'model', '*.rb')].sort.each { |file| require file }

@@inversion = Inversion.new

before do
  content_type :json
end

get '/' do
  content_type :json
  { :content => 'hello' }.to_json
end
